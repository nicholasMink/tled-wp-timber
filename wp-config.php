<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sy1k*2y?Qd)C0LT}?^gp`K[Gp|T=MpH&?CRlRuV]tC9KWt7<NVO!Z,C$xuxv/gr&');
define('SECURE_AUTH_KEY',  'U<{#W>SVM1cX`8W+1JV0,NpEC**KIljp%xHFhn^mJXM_+M=#Zk59dR}5F=YrdP31');
define('LOGGED_IN_KEY',    'Q4<VhC_ %oF(Y|$o4)ZJDuo:F|Dd1@p)Ym?k#bBqEZZQuxGaJBxGD/USf=Hy{<b)');
define('NONCE_KEY',        'K].R7`hx6Hf`S&2*pvQ~Z~I`3,cK&bwoc~Yj,(!r<u|#xuj=NHc ?=I^VR5w03HC');
define('AUTH_SALT',        '?T$m`lv=}hdkvTTd]S3<EdDSt,aP+At&PnZ8ai(5VLa0R2OvEHj|[p4wW&$l{1iN');
define('SECURE_AUTH_SALT', 'u/@(z$}4#?.OwzEb|fN;~1lXE a/D7Z-kDkbXc2a;7[| WNz(%sTNAih(QAB^TY0');
define('LOGGED_IN_SALT',   '|oQPW f~PZ3h@HXFYoc~6=l2YY<~GRc@1;gIT`h,:H>3k;R,<yUi]||IU!4]2wZ8');
define('NONCE_SALT',       '2+yAZqb];lYA2s|;=fvI#Y{&7YblgTv3[;#z{K=][cj1f/mT^[~^?Guj#Y@46n;:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
