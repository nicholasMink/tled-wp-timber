'use strict';

var video = document.getElementById('videoPlayer');
var source = document.createElement('source');

var curriculumDevelopmentBtn = document.getElementById('curriculum_development_btn');
var curriculumDevelopmentSubmenu = document.getElementById('curriculum_development_submenu');
var curriculumDevelopmentMenu = new MenuButton(curriculumDevelopmentBtn, curriculumDevelopmentSubmenu);

var instructionalDevelopmentBtn = document.getElementById('instructional_development_btn');
var instructionalDevelopmentSubmenu = document.getElementById('instructional_development_submenu');
var instructionalDevelopmentMenu = new MenuButton(instructionalDevelopmentBtn, instructionalDevelopmentSubmenu);

var facultyDevelopmentBtn = document.getElementById('faculty_development_btn');
var facultyDevelopmentSubmenu = document.getElementById('faculty_development_submenu');
var facultyDevelopmentMenu = new MenuButton(facultyDevelopmentBtn, facultyDevelopmentSubmenu);

var supportStudentBtn = document.getElementById('support_student_btn');
var supportStaffBtn = document.getElementById('support_staff_btn');
var supportStudent = document.getElementById('support_student');
var supportStaff = document.getElementById('support_staff');

var supportStudentBlock = new ToggleContent(supportStudent);
var supportStaffBlock = new ToggleContent(supportStaff);

var supportStudentHeight = supportStudent.offsetHeight;
var supportStaffHeight = supportStaff.offsetHeight;

var lastfocus = null;

// var vidsmall = '/video/TLEDTrail1.mp4';
// var vidmed = vidsmall;
// var vidbig = vidsmall;

// Modernizr.on('videoautoplay', result => {
//   if (result && window.matchMedia('(max-width: 480px)').matches) {
//     source.setAttribute('src', vidsmall);
//     source.setAttribute('type', 'video/mp4');
//   } else if (result && window.matchMedia('(max-width: 720px)').matches) {
//     source.setAttribute('src', vidmed);
//     source.setAttribute('type', 'video/mp4');
//   } else if (result && window.matchMedia('(min-width: 721px)').matches) {
//     source.setAttribute('src', vidbig);
//     source.setAttribute('type', 'video/mp4');
//   }

//   video.appendChild(source);

//   video.load();

//   video.addEventListener('loadeddata', () => {
//     video.classList.add('playing');
//     video.play();
//     // wait a certain amount after video is finished loading
//     // window.setTimeout(() => {
//     // }, 1000);
//   });
// });

TweenLite.set(supportStudent, {
    opacity: 0,
    height: 0
});

TweenLite.set(supportStaff, {
    opacity: 0,
    height: 0
});

supportStaffBtn.addEventListener('click', function() {
    supportStudentBlock.close();
    supportStaffBlock.toggle();
});

supportStaffBtn.addEventListener('focusin', function() {
    supportStudentBlock.close();
    facultyDevelopmentSubmenu.parentElement.classList.remove('active');
});

supportStudentBtn.addEventListener('click', function() {
    supportStaffBlock.close();
    supportStudentBlock.toggle();
});

supportStudentBtn.addEventListener('focusin', function() {
    supportStaffBlock.close();
});

function ToggleContent(el) {
    var _this = this;

    var autoHeight = el.offsetHeight;

    this.toggle = function() {
        if (el.offsetHeight === 0) {
            _this.open();
        } else {
            _this.close();
        }
    };

    this.open = function() {
        el.classList.add('active');
        TweenLite.to(el, 0.3, {
            ease: Power4.easeOut,
            css: {
                opacity: 1,
                height: autoHeight + 'px'
            }
        });
    };

    this.close = function() {
        TweenLite.to(el, 0.3, {
            ease: Power4.easeOut,
            css: {
                opacity: 0,
                height: 0
            },
            onComplete: function onComplete() {
                el.classList.remove('active');
            }
        });
    };
}

function MenuButton(btn, el) {
    btn.addEventListener('click', toggleMenu);

    // btn.addEventListener('mouseenter', () => {
    //   el.parentElement.classList.add('hover');
    //   el.parentElement.classList.add('active');
    // });

    // btn.addEventListener('mouseleave', () => {
    //   el.parentElement.classList.remove('hover');
    // });

    // el.parentElement.addEventListener('mouseleave', () => {
    //   el.parentElement.classList.remove('active');
    // });

    el.parentElement.addEventListener('focusin', function() {
        if (lastfocus && lastfocus !== el) {
            console.log('we have a focus mismatch');
            lastfocus.parentElement.classList.remove('active');
        }
        // console.log('lastfocus: ', el.id);
    });

    el.parentElement.addEventListener('focusout', function() {
        lastfocus = el;
    });

    function toggleMenu() {
        el.parentElement.classList.toggle('active');
    }

    function showMenu() {
        el.parentElement.classList.add('active');
        console.log('menu is active');
    }

    function hideMenu() {
        el.parentElement.classList.remove('active');
        console.log('menu is butt');
    }
}